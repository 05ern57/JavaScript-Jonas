const age = 15;
//const isOldEnough = age >= 18;

if (age >= 18) {
  // if age would under 18 then this block would never be executed and it would
  // jump into else part to execute the else block
  console.log("Sarah can start driving license ");
} else {
  const yearsLeft = 18 - age;
  console.log(`${yearsLeft} years left Sarah to get license`);
}
