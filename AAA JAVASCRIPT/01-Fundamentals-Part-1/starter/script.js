let js = "amazing";

console.log(5 + 7 + -1);
console.log(25);

let sayMyName = "Eren";

// let $ = "my little pony";

// console.log($);

//you can not use new keyword because it's reserved
//let new = 2424412424;

//reserved keywords function - if - else -bla bla bla.

const now = 2037;
const ageJonas = now - 1991;
const ageEren = now - 2003;
console.log(ageJonas * 2, ageEren ** 2, ageEren / 2);
// (2 ** 3) means exponent of 2 is 3 = 2 * 2 * 2 = 8

const firstName = "Eren";
const lastName = "Erdoğan";

let num = 5;

console.log(num + "Adet ");
let x = 5;
x += 10;
x *= 4;
x++;
x--;
x++;
console.log(x);

let _12 = 12;
let _15 = 15;

console.log(_12 >= _15);
console.log(_12 <= _15);

//template String/Literal ==> bu stringler içerisine yer tutucu ifadeler girilebilir bu ifadeler değişkenler olabilir.
console.log(`merhaba benim adım ${firstName} ve soyadım ${lastName}`);

//ECMAS5 version of the example above
console.log(" merhaba benim adım " + firstName + " ve soyadım " + lastName);

//Template Literal/String ECMAS5
console.log(
  "String with \n\
mutiple \n\
lines"
);

//ECMAS6 version  of the example
console.log(`String with 
multiple
lines`);
