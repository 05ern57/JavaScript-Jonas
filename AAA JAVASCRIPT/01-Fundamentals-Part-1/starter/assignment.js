// let mer = "name";

// let haba = false;
// mer = 12354654;
// console.log(typeof true);
// console.log(typeof mer);
// console.log(typeof haba);
// console.log(typeof 123);
// console.log(typeof "insanlar");

// let basarisiz;

// console.log(typeof basarisiz);

let country = "Türkiye";
let continent = "Asia";
let population = 82 * 10 ** 6;
console.log(population);

let PI = 3.1415;

let isIsland = false;

let language;

// console.log(typeof language);
// console.log(typeof country);
// console.log(typeof continent);
// console.log(typeof population);

language = "Turkce";

console.log(population / 2);
population++;
console.log(population);

const finlandPopulation = 6000000;

console.log(finlandPopulation > population);

const avaragePop = 33000000;

console.log(population > avaragePop);

//precedence of '+' operator higher than the '>'
console.log(population + 35 > finlandPopulation + population);

const description =
  "Portugal is in Europe, and its 11 million people speak portuguese";
console.log(description);

const massMark = 78;
const heightMark = 1.69;
const massJohn = 92;
const heightJohn = 1.95;

const BMIMark = massMark / (heightMark * heightMark);
const BMIJohn = massJohn / (heightJohn * heightJohn);

console.log(BMIMark);
console.log(BMIJohn);

const markHigherBMI = BMIMark > BMIJohn;
